# vi: ts=4 expandtab
#
#    Author: Osvaldo Demo <osvaldo.demo@mosolutions.nz>
#    Author: Mourad Kessas  <mourad.kessas@mosolutions.nz>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3, as
#    published by the Free Software Foundation.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import random
import netifaces

import requests
import time

from cloudinit import version
from cloudinit import log as logging
from cloudinit import net as cloudnet
from cloudinit import util
from cloudinit.url_helper import CONFIG_ENABLED, \
    _get_ssl_args, _cleanurl, UrlResponse, UrlError, SSL_ENABLED
from requests import exceptions

LOG = logging.getLogger(__name__)
NIC_MAP = {'public': 'eth0', 'private': 'eth1'}


def assign_ipv4_link_local(nic=None):
    """Bring up NIC using an address using link-local (ip4LL) IPs.
       The link-local domain is per-vm routed, so there
       is no risk of collisions. However, to be more safe, the ip4LL
       address is random.
    """

    if not nic:
        for cdev in sorted(cloudnet.get_devicelist()):
            if cloudnet.is_physical(cdev):
                nic = cdev
                LOG.debug("assigned nic '%s' for link-local discovery", nic)
                break

    if not nic:
        raise RuntimeError("unable to find interfaces to access the"
                           "meta-data server. This VM is broken.")

    addr = "169.254.{0}.{1}/16".format(random.randint(1, 168),
                                       random.randint(0, 255))

    ip_addr_cmd = ['ip', 'addr', 'add', addr, 'dev', nic]
    ip_link_cmd = ['ip', 'link', 'set', 'dev', nic, 'up']

    if not util.which('ip'):
        raise RuntimeError("No 'ip' command available to configure ip4LL "
                           "address")

    try:
        (result, _err) = util.subp(ip_addr_cmd)
        LOG.debug("assigned ip4LL address '%s' to '%s'", addr, nic)

        (result, _err) = util.subp(ip_link_cmd)
        LOG.debug("brought device '%s' up", nic)
    except Exception as e:
        util.logexc(LOG, "ip4LL address assignment of '%s' to '%s' failed."
                         " VM networking will be broken", addr, nic, e)
        raise

    return nic


def del_ipv4_link_local(nic=None):
    """Remove the ip4LL address. While this is not necessary, the ip4LL
       address is extraneous and confusing to users.
    """
    if not nic:
        LOG.debug("no link_local address interface defined, skipping link "
                  "local address cleanup")
        return

    LOG.debug("cleaning up ipv4LL address")

    ip_addr_cmd = ['ip', 'addr', 'flush', 'dev', nic]

    try:
        (result, _err) = util.subp(ip_addr_cmd)
        LOG.debug("removed ip4LL addresses from %s", nic)

    except Exception as e:
        util.logexc(LOG, "failed to remove ip4LL address from '%s'.", nic, e)


def convert_network_configuration(config, dns_servers):
    """Convert Network description into Cloud-init's netconfig
       format.

       Example JSON:
        {'public': [
              {'mac': '04:01:58:27:7f:01',
               'ipv4': {'gateway': '45.55.32.1',
                        'netmask': '255.255.224.0',
                        'ip_address': '45.55.50.93'},
               'anchor_ipv4': {
                        'gateway': '10.17.0.1',
                        'netmask': '255.255.0.0',
                        'ip_address': '10.17.0.9'},
               'type': 'public',
               'ipv6': {'gateway': '....',
                        'ip_address': '....',
                        'cidr': 64}}
           ],
          'private': [
              {'mac': '04:01:58:27:7f:02',
               'ipv4': {'gateway': '10.132.0.1',
                        'netmask': '255.255.0.0',
                        'ip_address': '10.132.75.35'},
               'type': 'private'}
           ]
        }
    """

    def _get_subnet_part(pcfg, nameservers=None):
        subpart = {'type': 'static',
                   'control': 'auto',
                   'address': pcfg.get('ip_address'),
                   'gateway': pcfg.get('gateway')}

        if nameservers:
            subpart['dns_nameservers'] = nameservers

        if ":" in pcfg.get('ip_address'):
            subpart['address'] = "{0}/{1}".format(pcfg.get('ip_address'),
                                                  pcfg.get('cidr'))
        else:
            subpart['netmask'] = pcfg.get('netmask')

        return subpart

    all_nics = []
    for k in ('public', 'private'):
        if k in config:
            all_nics.extend(config[k])

    macs_to_nics = cloudnet.get_interfaces_by_mac()
    nic_configs = []

    for nic in all_nics:

        mac_address = nic.get('mac')
        sysfs_name = macs_to_nics.get(mac_address)
        nic_type = nic.get('type', 'unknown')
        # Note: the entry 'public' above contains a list, but
        # the list will only ever have one nic inside it.
        # If it ever had more than one nic, then this code would
        # assign all 'public' the same name.
        if_name = NIC_MAP.get(nic_type, sysfs_name)

        LOG.debug("mapped %s interface to %s, assigning name of %s",
                  mac_address, sysfs_name, if_name)

        ncfg = {'type': 'physical',
                'mac_address': mac_address,
                'name': if_name}

        subnets = []
        for netdef in ('ipv4', 'ipv6', 'anchor_ipv4', 'anchor_ipv6'):
            raw_subnet = nic.get(netdef, None)
            if not raw_subnet:
                continue

            sub_part = _get_subnet_part(raw_subnet)
            if nic_type == 'public' and 'anchor' not in netdef:
                # add DNS resolvers to the public interfaces only
                sub_part = _get_subnet_part(raw_subnet, dns_servers)
            else:
                # remove the gateway any non-public interfaces
                if 'gateway' in sub_part:
                    del sub_part['gateway']

            subnets.append(sub_part)

        ncfg['subnets'] = subnets
        nic_configs.append(ncfg)
        LOG.debug("nic '%s' configuration: %s", if_name, ncfg)

    return {'version': 1, 'config': nic_configs}


def read_metadata(url, data, timeout=2, sec_between=2, retries=30):
    # POST data and force content-type to JSON
    LOG.debug('data: ' + str(data))
    response = readurl(url,
                       data=data,
                       timeout=timeout,
                       sec_between=sec_between, retries=retries,
                       )

    if not response.ok():
        raise RuntimeError("unable to read metadata at %s" % url)
    return json.loads(response.contents.decode())


def read_sysinfo():
    vendor_name = util.read_dmi_data("system-manufacturer")
    if vendor_name != "ezCloud":
        return (False, None)

    vm_id = util.read_dmi_data("system-serial-number")
    if vm_id:
        LOG.debug("system identified via SMBIOS as ezCloud vm id: %s",
                  vm_id)
    else:
        msg = ("system identified via SMBIOS as a ezCloud "
               "VM, but did not provide an ID. Please file a "
               "support ticket at: "
               "https://www.ezcloud.nz/support/tickets/new")

        LOG.critical(msg)
        raise RuntimeError(msg)

    return True, vm_id


def get_mac_address(ifname):
    """
    Retrieve interface physical address (MAC)
    :param ifname:
    :return:
    """
    if ifname in netifaces.interfaces():
        return netifaces.ifaddresses(ifname)[netifaces.AF_LINK][0]['addr']
    else:
        return 'FF:FF:FF:FF:FF:FF'


def readurl(url, data=None, timeout=None, retries=0, sec_between=1,
            headers=None, headers_cb=None, ssl_details=None,
            check_status=True, allow_redirects=True, exception_cb=None):
    """
    Borrowed from url_helper.py

    Modified to allow POST calls with JSON data

    :param url:
    :param data:
    :param timeout:
    :param retries:
    :param sec_between:
    :param headers:
    :param headers_cb:
    :param ssl_details:
    :param check_status:
    :param allow_redirects:
    :param exception_cb:
    :return:
    """
    url = _cleanurl(url)
    req_args = {
        'url': url,
    }
    req_args.update(_get_ssl_args(url, ssl_details))
    req_args['allow_redirects'] = allow_redirects
    req_args['method'] = 'GET'
    if timeout is not None:
        req_args['timeout'] = max(float(timeout), 0)
    if data:
        req_args['method'] = 'POST'
    # It doesn't seem like config
    # was added in older library versions (or newer ones either), thus we
    # need to manually do the retries if it wasn't...
    if CONFIG_ENABLED:
        req_config = {
            'store_cookies': False,
        }
        # Don't use the retry support built-in
        # since it doesn't allow for 'sleep_times'
        # in between tries....
        # if retries:
        #     req_config['max_retries'] = max(int(retries), 0)
        req_args['config'] = req_config
    manual_tries = 1
    if retries:
        manual_tries = max(int(retries) + 1, 1)

    def_headers = {
        'User-Agent': 'Cloud-Init/%s' % (version.version_string()),
        'Content-Type': 'application/json'
    }
    if headers:
        def_headers.update(headers)
    headers = def_headers

    if not headers_cb:
        def _cb(url):
            return headers

        headers_cb = _cb
    if data:
        req_args['json'] = data
    if sec_between is None:
        sec_between = -1

    excps = []
    # Handle retrying ourselves since the built-in support
    # doesn't handle sleeping between tries...
    for i in range(0, manual_tries):
        req_args['headers'] = headers_cb(url)
        filtered_req_args = {}
        for (k, v) in req_args.items():
            if k == 'data':
                continue
            filtered_req_args[k] = v
        try:
            LOG.debug("[%s/%s] open '%s' with %s configuration", i,
                      manual_tries, url, filtered_req_args)

            r = requests.request(**req_args)
            if check_status:
                r.raise_for_status()
            LOG.debug("Read from %s (%s, %sb) after %s attempts", url,
                      r.status_code, len(r.content), (i + 1))
            # Doesn't seem like we can make it use a different
            # subclass for responses, so add our own backward-compat
            # attrs
            return UrlResponse(r)
        except exceptions.RequestException as e:
            if (isinstance(e, (exceptions.HTTPError)) and
                    hasattr(e, 'response') and  # This appeared in v 0.10.8
                    hasattr(e.response, 'status_code')):
                excps.append(UrlError(e, code=e.response.status_code,
                                      headers=e.response.headers,
                                      url=url))
            else:
                excps.append(UrlError(e, url=url))
                if SSL_ENABLED and isinstance(e, exceptions.SSLError):
                    # ssl exceptions are not going to get fixed by waiting a
                    # few seconds
                    break
            if exception_cb and exception_cb(req_args.copy(), excps[-1]):
                # if an exception callback was given it should return None
                # a true-ish value means to break and re-raise the exception
                break
            if i + 1 < manual_tries and sec_between > 0:
                LOG.debug("Please wait %s seconds while we wait to try again",
                          sec_between)
                time.sleep(sec_between)
    if excps:
        raise excps[-1]
    return None  # Should throw before this...
