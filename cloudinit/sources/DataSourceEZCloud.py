# vi: ts=4 expandtab
#
#    Author: Osvaldo Demo <osvaldo.demo@mosolutions.nz>
#    Author: Mourad Kessas  <mourad.kessas@mosolutions.nz>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3, as
#    published by the Free Software Foundation.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

from cloudinit import log as logging
from cloudinit import sources
from cloudinit import util

import cloudinit.sources.helpers.ezcloud as do_helper

LOG = logging.getLogger(__name__)

BUILTIN_DS_CONFIG = {
    'metadata_url': 'http://169.254.169.254/metadata/v1.json',
}

# Wait for a up to a minute, retrying the meta-data server
# every 2 seconds.
MD_RETRIES = 30
MD_TIMEOUT = 2
MD_WAIT_RETRY = 2
MD_USE_LL_IPV4 = True


class DataSourceEZCloud(sources.DataSource):
    def __init__(self, sys_cfg, distro, paths):
        sources.DataSource.__init__(self, sys_cfg, distro, paths)
        self.distro = distro
        self.metadata = dict()
        self.ds_cfg = util.mergemanydict([
            util.get_cfg_by_path(sys_cfg, ["datasource", "EZCloud"], {}),
            BUILTIN_DS_CONFIG])
        self.metadata_address = self.ds_cfg['metadata_url']
        self.retries = self.ds_cfg.get('retries', MD_RETRIES)
        self.timeout = self.ds_cfg.get('timeout', MD_TIMEOUT)
        self.use_ip4LL = self.ds_cfg.get('use_ip4LL', MD_USE_LL_IPV4)
        self.wait_retry = self.ds_cfg.get('wait_retry', MD_WAIT_RETRY)
        self._network_config = None

    def _get_sysinfo(self):
        return do_helper.read_sysinfo()

    def get_data(self):
        (is_ezcloud, vm_id) = self._get_sysinfo()

        # only proceed if we know we are on ezCloud
        if not is_ezcloud:
            return False

        LOG.info("Running on ezCloud. vm_id=%s" % vm_id)

        ipv4_LL = None
        if self.use_ip4LL:
            ipv4_LL = do_helper.assign_ipv4_link_local()

        # Retrieve NIC physical address.
        # eth0 will always be used for this.
        mac = do_helper.get_mac_address('eth0')

        LOG.debug('vm_id: ' + str(vm_id))
        LOG.debug('mac: ' + str(mac))

        md = do_helper.read_metadata(
            self.metadata_address,
            data={'vm_id': vm_id,
                  'mac': mac},
            timeout=self.timeout,
            sec_between=self.wait_retry,
            retries=self.retries
        )

        self.metadata_full = md
        self.metadata['instance-id'] = md.get('vm_id', vm_id)
        self.metadata['local-hostname'] = md.get('hostname', vm_id)
        self.metadata['interfaces'] = md.get('interfaces')
        self.metadata['public-keys'] = md.get('public_keys')
        self.metadata['availability_zone'] = md.get('region', 'default')
        self.vendordata_raw = md.get("vendor_data", None)
        self.userdata_raw = md.get("user_data", None)

        if ipv4_LL:
            do_helper.del_ipv4_link_local(ipv4_LL)

        return True

    def check_instance_id(self, sys_cfg):
        return sources.instance_id_matches_system_uuid(
            self.get_instance_id(), 'system-serial-number')

    @property
    def network_config(self):
        """Configure the networking. This needs to be done each boot, since
           the IP information may have changed due to snapshot and/or
           migration.
        """

        if self._network_config:
            return self._network_config

        interfaces = self.metadata.get('interfaces')
        LOG.debug('interfaces: ' + str(interfaces))
        if not interfaces:
            raise Exception("Unable to get meta-data from server....")

        nameservers = self.metadata_full['dns']['nameservers']
        self._network_config = do_helper.convert_network_configuration(
            interfaces, nameservers)
        return self._network_config


# Used to match classes to dependencies
datasources = [
    (DataSourceEZCloud, (sources.DEP_FILESYSTEM,)),
]


# Return a list of data sources that match this set of dependencies
def get_datasource_list(depends):
    return sources.list_from_depends(depends, datasources)
