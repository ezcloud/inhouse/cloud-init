# vi: ts=4 expandtab
#
#    Author: Osvaldo Demo <osvaldo.demo@easycloud.nz>
#    Author: Mourad Kessas  <mourad.kessas@easycloud.nz>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3, as
#    published by the Free Software Foundation.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Reset root
---
**Summary:** Wait for the root password reset flag and generate a new one

**Internal name:** ``cc_resetpwd``

**Module frequency:** per always

**Supported distros:** all


"""
import json
import string
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
from base64 import b64decode, b64encode
import random
import os.path
from cloudinit import log as logging, url_helper
from cloudinit import util
from cloudinit import ssh_util
from cloudinit.settings import PER_ALWAYS
from cloudinit.sources.DataSourceEZCloud import DataSourceEZCloud, \
    BUILTIN_DS_CONFIG
import cloudinit.sources.helpers.ezcloud as do_helper
from cloudinit.sources.helpers.ezcloud import read_metadata
from cloudinit.sources.helpers.ezcloud import read_sysinfo

frequency = PER_ALWAYS
LOG = logging.getLogger(__name__)


def generate_random_password(size):
    """
    Generates a random string to use as a password

    :param size: size of the generated random password
    :return:
    """

    options = string.ascii_uppercase + string.ascii_lowercase + string.digits

    return ''.join(random.SystemRandom().choice(options) for _ in range(size))


def send_password_data(vm_id, password, post_url):
    """
    Post call to the one time url backend services

    :param vm_id: VM id
    :param password: new password
    :param post_url: http url for POST
    :return:
    """
    LOG.debug('Issuing POST to endpoint[{0}] with data[{1}]'.format(
        post_url,
        {'password': password,
         'id': vm_id}))
    response = url_helper.readurl(post_url,
                                  data={'password': password,
                                        'id': vm_id},
                                  timeout=300,
                                  retries=10)

    if not response.ok():
        raise RuntimeError("Error when trying to POST data to %s" % post_url)

    return json.loads(response.contents.decode())


def handle(name, cfg, cloud, log, args):
    """
    Entry point for the stage to run the module

    :param name:
    :param cfg:
    :param cloud:
    :param log:
    :param args:
    :return:
    """

    if isinstance(cloud.datasource, DataSourceEZCloud):
        log.info('Running module {0}'.format(str(name)))
        log.info('Using EZCloud as datasource. '
                 'Module will check for the password reset flag!')

        log.info('Retrieving metadata from {0}'.format(str(cloud.datasource)))
        (is_ezcloud, vm_id) = read_sysinfo()
        # only proceed if we know we are on ezCloud
        if not is_ezcloud:
            LOG.info("Not running on ezCloud! Aborting...")
            return

        LOG.info("Running on ezCloud. vm_id=%s" % vm_id)

        mac = do_helper.get_mac_address('eth0')

        metadata = read_metadata(
            BUILTIN_DS_CONFIG['metadata_url'],
            data={'vm_id': vm_id,
                  'mac': mac},
        )

        # Values for the module to work properly
        reset_flag = metadata['password']['reset']
        pubkey = metadata['password']['public_key']
        post_url = metadata['password']['post_url']

        log.info('Checking for password reset flag.')
        if not reset_flag:
            log.info('Flag is not set. Root password '
                     'will not be reset this time.')
            return
        else:
            log.info('Reset Flag is set. Proceeding '
                     'to reset root password.')

        size = util.get_cfg_option_str(cfg, "password_size", 32)
        log.info('Generating new random password consisting '
                 'of {0} characters and/or numbers.'.format(size))
        new_password = generate_random_password(size)
        # log.debug('Password size: ' + str(size))
        # log.debug('Generated password: ' + str(new_password))

        log.info('Encrypting new password with public key...')
        # log.debug('public_key: ' + str(pubkey))
        encrypted = encrypt_password_rsa(pubkey, new_password)
        new_vm_id = encrypt_password_rsa(pubkey, vm_id)
        # log.debug('encrypted: ' + str(encrypted))

        log.info('Posting new password to the backend services...')
        response = None
        try:
            response = send_password_data(new_vm_id, encrypted, post_url)
        except Exception as e:
            log.debug('Failed to send new password to backend services...')
            log.debug('ERROR: ' + str(e))

        if response is not None:
            log.debug(
                'Backend services response. Status: {0}, '
                'Message: {1}'.format(response['status'], response['details']))

        # Change the password in the system
        expire = True
        pw_auth = "yes"
        change_pwauth = True
        plist = "root:%s" % new_password
        errors = []
        plist_in = []
        users = []
        for line in plist.splitlines():
            u, p = line.split(':', 1)
            plist_in.append("%s:%s" % (u, p))
            users.append(u)

        ch_in = '\n'.join(plist_in) + '\n'
        try:
            log.debug("Changing password for %s:", users)
            util.subp(['chpasswd'], ch_in)
        except Exception as e:
            errors.append(e)
            util.logexc(log, "Failed to set passwords with chpasswd for %s",
                        users)

        if expire:
            expired_users = []
            for u in users:
                try:
                    util.subp(['passwd', '--expire', u])
                    expired_users.append(u)
                except Exception as e:
                    errors.append(e)
                    util.logexc(log, "Failed to set 'expire' for %s", u)
            if expired_users:
                log.debug("Expired passwords for: %s users", expired_users)

        # Update ssh configuration to allow password based logins
        if change_pwauth:
            replaced_auth = False

            # See: man sshd_config
            old_lines = ssh_util.parse_ssh_config(ssh_util.DEF_SSHD_CFG)
            new_lines = []
            i = 0
            for (i, line) in enumerate(old_lines):
                # Keywords are case-insensitive and
                # arguments are case-sensitive
                if line.key == 'passwordauthentication':
                    log.debug("Replacing auth line %s with %s", i + 1, pw_auth)
                    replaced_auth = True
                    line.value = pw_auth
                new_lines.append(line)

            if not replaced_auth:
                log.debug("Adding new auth line %s", i + 1)
                replaced_auth = True
                new_lines.append(ssh_util.SshdConfigLine(
                    '',
                    'PasswordAuthentication',
                    pw_auth))

            lines = [str(e) for e in new_lines]
            util.write_file(ssh_util.DEF_SSHD_CFG, "\n".join(lines))

            # Replace PermitRootLogin config stanza
            try:
                util.subp(['sed', '-i', '-e', "/^PermitRootLogin/s/"
                                              "^.''*$/PermitRootLogin yes/",
                           ssh_util.DEF_SSHD_CFG])
            except Exception as e:
                errors.append(e)
                util.logexc(log, "Failed to set "
                                 "PermitRootLogin config stanza...")

            try:
                cmd = cloud.distro.init_cmd  # Default service
                cmd.append(cloud.distro.get_option('ssh_svcname', 'ssh'))
                cmd.append('restart')
                if 'systemctl' in cmd:  # Switch action ordering
                    cmd[1], cmd[2] = cmd[2], cmd[1]
                cmd = filter(None, cmd)  # Remove empty arguments
                util.subp(cmd)
                log.debug("Restarted the ssh daemon")
            except:
                util.logexc(log, "Restarting of the ssh daemon failed")

            authorized_keys_file = [
                '/root/.ssh/authorized_keys',
                '/root/.ssh/authorized_keys2'
            ]

            for file_path in authorized_keys_file:
                if os.path.exists(file_path):
                    # Move root's authorized keys file
                    try:
                        util.logexc(log, 'Moving {0} '
                                         'file to /root/'.format(file_path))
                        util.subp(['mv', '-f', file_path, '/root/'])
                    except Exception as e:
                        errors.append(e)
                        util.logexc(log, 'Failed to '
                                         'move {0} file'.format(file_path))

        if len(errors):
            log.debug("%s errors occured, "
                      "re-raising the last one", len(errors))
            raise errors[-1]

    else:
        log.info('Not using EZCloud datasource. '
                 'Module will not run!')
        log.info('Module is only compatible with '
                 'EZCloud instances and infrastructure.')


def encrypt_password_rsa(public_key, message):
    """

    Encrypt message using RSA public key with PKCS1-OAEP padding

    param: public_key Path to public key
    param: message String to be encrypted
    return base64 encoded encrypted string
    """

    # LOG.debug('key: ' + str(public_key))
    # LOG.debug('message: ' + str(message))
    rsa_key = RSA.importKey(public_key.encode())
    rsakey = PKCS1_OAEP.new(rsa_key)
    encrypted = rsakey.encrypt(message.encode())
    return b64encode(encrypted)


def decrypt_password_rsa(private_key, package):
    """

    Decrypt a package using RSA private key with PKCS1-OAEP padding

    param: public_key_loc Path to your private key
    param: package String to be decrypted
    return decrypted string
    """

    rsa_key = RSA.importKey(private_key.encode())
    rsakey = PKCS1_OAEP.new(rsa_key)
    decrypted = rsakey.decrypt(b64decode(package))

    return decrypted
